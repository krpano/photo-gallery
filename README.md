# krpano photo-gallery

    A small addition, which is a simple adaptive photo gallery.

## Syntax

    <include url="plugins/photo-gallery.xml" />

	<photogallery width="-50" height="-120" maxwidth="1280" maxheight="">

		<title
			css.normal="font-family:Monospace; font-size:12px; color:0xFFFFFF; padding:10px"
			css.mobile="font-family:Monospace; font-size:11px; color:0xFFFFFF; padding:10px"
			bgalpha="0.5"
			bgcolor="0x000000"
		/>
	</photogallery>
    
    <hotspot name="hs-1" style="hs" ... image="photos/1.jpg" title="data:d1" />
    <hotspot name="hs-2" style="hs" ... image="photos/2.jpg" title="data:d2" />
    <hotspot name="hs-3" style="hs" ... image="photos/3.jpg" title="descrip" />

    <style name="hs" onclick="loadphoto()" />

## Settings

    width / height - indents with respect to the browser window

    maxwidth / maxheight - limit the maximum size of the gallery popup in pixels

    title:

        css - the style of your HTML text

        bgalpha / bgcolor - transparency and color of the text field

## Attributes

    image - path to the image you want to open

    title - text description for image.

## Actions

    loadphoto - load image and text that were passed in attributes