# 1.0.0
- First version

# 1.0.1
- Fix: after hiding the gallery, the link remains available for clicking

# 1.0.2
- Feature: the maximum size of a photo can not be larger than the size of the source

# 1.0.3
- Feature: added custom settings (width, height, maxwidth and maxheight)

# 1.0.4
- Feature: added new user settings (title.css, title.bgalpha, title.bgcolor)